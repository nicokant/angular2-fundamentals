import { Injectable } from '@angular/core';

@Injectable()
export class MailService {

  message = `you've got mail`;

  messageList = [
    'angular newsletter',
    'message from space',
    'welcome to my website'
  ]

  constructor() { }

}
